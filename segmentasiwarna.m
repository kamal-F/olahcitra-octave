Img = imread('./Images/segwarn.jpg');
figure, imshow(Img), title('original image');

[tinggi,lebar,~] = size(Img);
hsv = rgb2hsv(Img);
 
H = hsv(:,:,1);
S = hsv(:,:,2);
V = hsv(:,:,3);
 
for y=1: tinggi
    for x=1: lebar
        h = H(y,x);
 
        % Ubah warna
        if h < 11/255       % merah
            h = 0;
        elseif h < 32/255   % jingga
            h = 21/255;
        elseif h < 54/255   % kuning
            h = 43/255;
        elseif h < 116/255  % hijau
            h = 85/255;
        elseif h < 141/255  % cyan
            h = 128/255;
        elseif h < 185/255  % biru
            h = 170/255;
        elseif h < 202/255  % ungu
            h = 191/255;
        elseif h < 223/255  % magenta
            h = 213/255;
        elseif h < 244/255  % merah muda
            h = 234/255;
        else
            h = 0;          % merah
        end
 
        % Ubah komponen H
        H(y,x) = h;
 
        % Ubah komponen S
        if S(y,x) >= 200/255
            S(y,x) = 255/255;
        elseif S(y,x) <= 20/255
            S(y,x) = 0;
        else
            S(y,x) = 128/255;
        end
 
        % Ubah komponen V
        if V(y,x) >= 200/255
            V(y,x) = 255/255;
        elseif V(y,x) <= 20/255
            V(y,x) = 0;
        else
            V(y,x) = 128/255;
        end
    end
end
 
% H_aksen = H==0/255;
% H_aksen = H==21/255;
% H_aksen = H==43/255;
 H_aksen = H==85/255;
% H_aksen = H==128/255;
% H_aksen = H==170/255;
% H_aksen = H==191/255;
% H_aksen = H==213/255;
% H_aksen = H==234/255;

H_aksen = logical(H_aksen);
 
R = Img(:,:,1);
G = Img(:,:,2);
B = Img(:,:,3);
 
R(~H_aksen) = 255;
G(~H_aksen) = 255;
B(~H_aksen) = 255;
 
RGB = cat(3,R,G,B);
 
figure, imshow(RGB), title('segmented image');
%imshow(RGB);