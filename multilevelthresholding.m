clc; clear; close all; warning off all;
  
%read the image
I = imread('./Images/citra-selena-gomez.jpg');
figure; imshow(I);

out = uint8(zeros(size(I,1), size(I,2), size(I,3)));
  
%R,G,B components of the input image
R = I(:,:,1);
G = I(:,:,2);
B = I(:,:,3);
  
%Inverse of the Avg values of the R,G,B
mR = 1/(mean(mean(R)));
mG = 1/(mean(mean(G)));
mB = 1/(mean(mean(B)));
  
%Smallest Avg Value (MAX because we are dealing with the inverses)
maxRGB = max(max(mR, mG), mB);
  
%Calculate the scaling factors
mR = mR/maxRGB;
mG = mG/maxRGB;
mB = mB/maxRGB;
  
%Scale the values
out(:,:,1) = R*mR;
out(:,:,2) = G*mG;
out(:,:,3) = B*mB;
  
%Convert the image from RGB to YCbCr
img_ycbcr = rgb2ycbcr(out);
figure, imshow(img_ycbcr);

Y = img_ycbcr(:,:,1);
Cb = img_ycbcr(:,:,2);
Cr = img_ycbcr(:,:,3);
  
figure, imshow(Y);
figure, imshow(Cb);
figure, imshow(Cr);

%Detect Skin
[r,c,v] = find(Cb>=77 & Cb<=127 & Cr>=133 & Cr<=193);
numind = size(r,1);
 
bin = false(size(I,1), size(I,2));

% Melakukan multi-level thresholding pada komponen Cb dan Cr 
%Mark Skin Pixels
for i=1:numind
    bin(r(i),c(i)) = 1;
end
 
figure, imshow(bin);

% Melakukan operasi morfologi berupa filling holes untuk mengisi region objek yang kosong
bin = imfill(bin,'holes');
figure, imshow(bin);

% Melakukan operasi morfologi berupa area opening untuk menghilangkan noise (region yang bukan objek)
bin = bwareaopen(bin,1000);
figure; imshow(bin);

% Memvisualisasikan hasil segmentasi kulit pada citra RGB
R(~bin) = 0;
G(~bin) = 0;
B(~bin) = 0;
out = cat(3,R,G,B);
figure; imshow(out);

% Memvisualisasikan hasil segmentasi kulit dalam bentuk bounding box
s = regionprops(bin,'BoundingBox');
bbox = cat(1, s.BoundingBox);
  
% RGB = insertShape(I, 'rectangle', bbox, 'LineWidth', 5);
% RGB = shapedraw(I, 'rectangle', bbox, 'LineWidth', 5);


figure, imshow(I);
rectangle ("Position", bbox, "EdgeColor", [0,1, 0]);

