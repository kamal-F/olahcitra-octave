clc;clear;close all;
 
I = imread('./Images/morph.png');
R = I(:,:,1);
G = I(:,:,2);
B = I(:,:,3);
Red = cat(3,R,G*0,B*0);
Green = cat(3,R*0,G,B*0);
Blue = cat(3,R*0,G*0,B);
 
figure, imshow(I);
figure, imshow(Red);
figure, imshow(Green);
figure, imshow(Blue);

% Persamaan yang umumnya digunakan untuk mengkonversi citra RGB truecolor 24-bit menjadi citra grayscale 8-bit adalah

% 0.2989*R+0.5870*G+0.1140*B  

% grayscale
J = rgb2gray(I);
figure, imshow(J);

% black white binary
K = im2bw(J,0.5);
figure, imshow(K);

% Otsu
L = graythresh(J);
M = im2bw(J,L);
figure, imshow(M);

